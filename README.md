    This project does the fetching of new emails from the mailbox of the 
provided email using IMAP and checks for the attachments. If found then 
attachment is processed to fill the data in the database from the attachment 
and send back email to same user who sent the attachment with the link to view 
all the data populated by him till date.